Nguyen Trong Thien - S3426515
Tran Vuong Trung - S3408675

HOW TO COMPILE AND RUN:

1. Put all the *.java files and the document and stoplist file into the same directory

2. Then we run this following command to compile the java files:
  javac Index.java Tag.java Search.java

3. After that we have the *.class files that we should be able to execute through command-line:
  java -cp . Index latimes
  java -cp . Search lexicon invlists map latimes <query-terms>

4. Note that the Search.class needs at least 5 arguments to run with the 4th argument being the path to the collection file latimes.