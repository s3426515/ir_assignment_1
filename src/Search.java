import java.io.*;
import java.lang.String;
import java.text.BreakIterator;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by TRUNG PC on 3/27/2017.
 */
public class Search {

    private String[] args;
    private HashMap<String, int[]> lexicon = new HashMap<>();
    private HashMap<Integer, double[]> mapping = new HashMap<>();
    private HashMap<Integer, ArrayList<String>> stopTable = new HashMap<>();
    private HashMap<Integer, String[]> tagList = new HashMap<>();
    private HashMap<Integer, String> qbSummary = new HashMap<>();
    private String pattern1 = "(?<!\\D)\\,(?!\\D)";
    private String pattern2 = "[^a-zA-Z0-9_.-]+|(?<!\\d)\\.(?!\\d)";
    private String pattern3 = "(?!\\D\\W)\\.(?!\\d\\w*)";
    private ExecutorService es = Executors.newCachedThreadPool();
    private boolean isText = false;
    private boolean isTag = false;
    private int numDocument = 0;
    private double avgDocLength = 0;
    private ArrayList<DocAc> heap;
    private int currentSize;
    private ArrayList<String> list1 = new ArrayList<>();
    private boolean outputDocSum = true;
    private String queryID = "";

    public static void main(java.lang.String[] args) {
        String input = "";
        Search search = new Search(args);
        Scanner scanner = new Scanner(System.in);
        while(!Objects.equals(input, "--exit")) {
            System.out.print("Pls enter a query: ");
            input = scanner.nextLine();
//            System.out.println("you entered: " + input);
            if (input.indexOf("--") != 0) {
                try {
                    search.parseQuery(input);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (input.indexOf("--ds ON") == 0) {
                search.setOutputDocSum(true);
            } else if (input.indexOf("--ds OFF") == 0) {
                search.setOutputDocSum(false);
            } else if (input.indexOf("--h") == 0) {
                search.printHelp();
            }
        }

    }

    public Search (java.lang.String[] args) {
        this.args = args;
        heap = new ArrayList<>();
        heap.add(0, new DocAc(Integer.MIN_VALUE, Integer.MIN_VALUE));
        init();
        tagList.put(0, new String[]{"<DOCNO>", "</DOCNO>"});
        tagList.put(1, new String[]{"<HEADLINE>", "</HEADLINE>"});
        tagList.put(2, new String[]{"<TEXT>", "</TEXT>"});
        list1.add("mr");
        list1.add("mrs");

    }

    private void init () {
        // process arguments
        if (args.length >= 4) {
            try {
                es.execute(() -> {
                    try {
                        readLexiconxFile(args[0]);
                        System.out.println("Leixcon loaded. There are " + lexicon.size() + " entries.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                es.execute(() -> {
                    try {
                        readMappingFile(args[2]);
                        System.out.println("Mapping loaded. There are " + mapping.size() + " documents.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                es.execute(() -> {
                    try {
                        readStopList("");
//                        System.out.println("Mapping loaded. There are " + mapping.size() + " documents.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                es.shutdown();
                es.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Not enough inputs");
        }
    }

    //read lexicon file and map file into memory
    private void readLexiconxFile (String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String []parts;
            String line;
            while ((line = br.readLine()) != null) {
                parts = line.split(",");
                lexicon.put(parts[0],new int[2]);
                lexicon.get(parts[0])[0] = Integer.parseInt(parts[1]);
                lexicon.get(parts[0])[1] = Integer.parseInt(parts[2]);
            }
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readMappingFile(String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String []parts;
            String line;
            int mappingTableColumns = 5;
            double totalLength = 0;
            while ((line = br.readLine()) != null) {
                parts = line.split(",");
                mapping.put(Integer.parseInt(parts[0]),new double[mappingTableColumns]);
                numDocument++;
                for (int a = 0; a < mappingTableColumns; a++) {
                    mapping.get(Integer.parseInt(parts[0]))[a] = Double.parseDouble(parts[a+1]);
                    if (a == 3) totalLength += Double.parseDouble(parts[a+1]);
                }
            }
            avgDocLength = totalLength / numDocument;
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readStopList(String file) throws IOException{
        if (file.equals("")) { file = "stoplist"; }
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                int charCode = (int) line.charAt(0);
                if (stopTable.containsKey(charCode))
                    stopTable.get(charCode).add(line);
                else {
                    stopTable.put(charCode, new ArrayList<>());
                    stopTable.get(charCode).add(line);
                }
            }
        }
    }

    private void printHelp() {
        System.out.println("--h //Show available commands");
        System.out.println("--exit //Exit the program");
        System.out.println("--ds ON //Turn on document summary");
        System.out.println("--ds OFF //Turn off document summary");
        System.out.println("-<QUERY LABEL> //Add that in front of a query for query label");
    }

    private void parseQuery(String queryTerms) throws IOException {
        String query = queryTerms.replaceAll("\\<.*?>","");
        if (query.indexOf("-") == 0) {
            queryID = query.substring(query.indexOf((int) '-') + 1, query.indexOf((int) ' '));
            query = query.substring(query.indexOf((int) ' ') + 1, query.length());
        }
        query = query.replaceAll(pattern1, "");
        query = query.replaceAll(pattern3, "");
        query = query.replaceAll(pattern2, " ").toLowerCase();
        System.out.println("Searching for \"" + query + "\"");
        String[] arr = query.split(" ");

        rankedRetrieve(arr, 10);
    }

    private void setOutputDocSum(boolean b) {outputDocSum = b;}

    private HashMap<Integer, String[]> queryBiasDocSum(HashMap<Integer, Double> docList, String[] queryTerms) {
        File f = new File(args[3]);
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder tagBuilder = new StringBuilder();
        StringBuilder stringBuilder2 = new StringBuilder();
        HashMap<Integer, int[]> sentenceIdMap = new HashMap<>();
        HashMap<Integer, String[]> docSummary = new HashMap<>();
        int maxNumOfSentence = 3;
        int content, check;
        StringBuilder st = new StringBuilder();
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        if (f.exists() && f.canRead()) {
            for(HashMap.Entry<Integer, Double> entry : docList.entrySet()) {
                try {
                    isText = true;
                    isTag = false;
                    check = 0;
                    stringBuilder.setLength(0);
                    for (int i = 1; i <= 2; i++) {
                        FileInputStream inputStream = new FileInputStream(f);
                        inputStream.skip((long) mapping.get(entry.getKey())[i]);
                        while ((content = inputStream.read()) != -1) {
                                if (content == (int) '<') {
                                    isText = false;
                                    isTag = true;
                                    tagBuilder.setLength(0);
                                }
                                if (isText) {
                                    if (content == (int) '\n') {
                                        stringBuilder.append(' ');
                                    } else
                                        stringBuilder.append((char) content);
                                }
                                if (isText && check == 0){
                                    if (content == (int) '\n') {
                                        stringBuilder2.append(' ');
                                    } else
                                        stringBuilder2.append((char) content);
                                }
                                if (isTag) tagBuilder.append((char) content);
                                if (content == (int) '>') {
                                    isText = true;
                                    isTag = false;
                                    if (tagBuilder.indexOf("</P>") > -1 && i == 2) check = 1;
                                    if (tagBuilder.indexOf(tagList.get(i)[1]) > -1) break;
                                }
                        }
                        inputStream.close();
                    }
                } catch (IOException io) {
                    io.printStackTrace();
                }

                BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.ENGLISH);
                iterator.setText(stringBuilder.toString());
                int start = iterator.first();
                sentenceIdMap.clear();
                int sentenceID = 0;
                int i = 0;
                resetHeap();
                st.setLength(0);
                for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
                    sentenceIdMap.put(sentenceID, new int[]{start, end});
                    double sentenceScore = getSentenceScore(stringBuilder.substring(start, end), queryTerms);
                    if (i < maxNumOfSentence) {
                        insert(new DocAc(sentenceID, sentenceScore));
                        i++;
                    }
                    else {
                        if (heap.get(1).Accumulator < sentenceScore) {
                            heap.set(1, new DocAc(sentenceID, sentenceScore));
                            minHeapify(1);
                        }
                    }
                    sentenceID++;
                }
                heap.sort((o1, o2) -> (o1.Accumulator >= o2.Accumulator)? -1 : 1 );
                st.append("...");
                for (int j = 0; j < heap.size()-1; j++) {
                    st.append(stringBuilder.substring(sentenceIdMap.get(heap.get(j).DocID)[0], sentenceIdMap.get(heap.get(j).DocID)[1]));
                    st.append("...");
                }
                docSummary.put(entry.getKey(), new String[2]);
                docSummary.get(entry.getKey())[0] = st.toString();
                docSummary.get(entry.getKey())[1] = stringBuilder2.toString();

                stringBuilder.setLength(0);
                stringBuilder2.setLength(0);
                tagBuilder.setLength(0);
            }
            resetHeap();
            return docSummary;
        } else {
            System.out.println("latimes doesn't exist or can't be read");
            return null;
        }
    }

    private double getSentenceScore(String sentence, String[] queryTerms) {
        String temp = sentence.replaceAll(pattern1, "");
        temp = temp.replaceAll(pattern3, "");
        temp = temp.replaceAll(pattern2, " ").toLowerCase();
        double uniqueQT = 0;
        for (String s : queryTerms) {
            if (temp.indexOf(s) >= 0) uniqueQT += 1.0;
            if (uniqueQT >= queryTerms.length) break;
        }
        double result = (uniqueQT * uniqueQT / queryTerms.length);
        return result;
    }

    private void rankedRetrieve(String[] queryTerms, int numberOfResults) throws IOException {

        HashMap<Integer, Double> docRanks = calculateDocRanking(getDocumentFromTerms(queryTerms));
        HashMap<Integer, String[]> docSummary = queryBiasDocSum(docRanks, queryTerms);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        if (docRanks.isEmpty())
            System.out.println("No result was found");
        else {
            int i = 0;
            for (HashMap.Entry<Integer, Double> entry : docRanks.entrySet()) {
                if (i < numberOfResults) {
                    insert(new DocAc(entry.getKey(), entry.getValue()));
                    i++;
                } else {
                    if (heap.get(1).Accumulator < entry.getValue()) {
                        heap.set(1, new DocAc(entry.getKey(), entry.getValue()));
                        minHeapify(1);
                    }
                }
            }
            heap.sort((o1, o2) -> (o1.Accumulator >= o2.Accumulator) ? -1 : 1);

            for (int j = 0; j < heap.size() - 1; j++) {
                System.out.println("\n=====================");
                System.out.println(queryID + readText(args[3], heap.get(j).DocID, 0) + " " + (j + 1) + " " + df.format(heap.get(j).Accumulator));
                if (outputDocSum) {
                    System.out.println();
                    System.out.println(docSummary.get(heap.get(j).DocID)[0]);
                    System.out.println();
                    System.out.println(docSummary.get(heap.get(j).DocID)[1]);
                }
            }
        }
        System.out.println("\n=====================\n\n\n\n");
        resetHeap();
    }

    private void resetHeap() {
        currentSize = 0;
        heap.clear();
        heap.add(0, new DocAc(Integer.MIN_VALUE, Integer.MIN_VALUE));
    }

    private HashMap<Integer, HashMap<String, Integer>> getDocumentFromTerms(String[] queryTerms) throws IOException {
        final int CHAR_SIZE = 4;
        int seekPos;
        int docf;
        RandomAccessFile randomFile = new RandomAccessFile(args[1], "r");
        HashMap<Integer, HashMap<String, Integer>> docList = new HashMap<>();

        for (String query : queryTerms){
            if (lexicon.containsKey(query)) {
                // use the pointer given by lexicon entry to find the location of term's inverted list
                int pointer = lexicon.get(query)[1];
                seekPos = CHAR_SIZE * pointer;
                randomFile.seek(seekPos);
                docf = randomFile.readInt();

                for (int i = 0; i < docf; i++) {
                    int docID = randomFile.readInt();
                    int inDocFreq = randomFile.readInt();
                    if (docList.containsKey(docID)) {
                        docList.get(docID).put(query, inDocFreq);
                    } else {
                        docList.put(docID, new HashMap<>());
                        docList.get(docID).put(query, inDocFreq);
                    }
                }
            }
        }

        return docList;
    }

    private HashMap<Integer, Double> calculateDocRanking(HashMap<Integer, HashMap<String, Integer>> docList) {
        HashMap<Integer, Double> docRanks = new HashMap<>();
        for (HashMap.Entry<Integer, HashMap<String, Integer>> entry : docList.entrySet()) {
            double bm25 = 0,
                    collectionSize = numDocument,
                    ft,
                    fdt,
                    k1 = 1.2,
                    b = 0.75,
                    adl = avgDocLength,
                    ld = mapping.get(entry.getKey())[3];
            for (HashMap.Entry<String, Integer> e : entry.getValue().entrySet()) {
                ft = lexicon.get(e.getKey())[0];
                fdt = e.getValue();
                bm25 += ((collectionSize - ft + 0.5) / (ft + 0.5)) * (k1 + 1) * fdt / ((k1 * (1 - b + (b * ld / adl))) + fdt);
            }
            docRanks.put(entry.getKey(), bm25);
        }

        return docRanks;
    }

    private void minHeapify(int pos) {
        int child_pos = leftChild(pos);
        while (child_pos < heap.size()) {
            //System.out.printf("Left: " + leftChild(pos) + " Right: " + rightChild(pos));
            if (child_pos < heap.size() && rightChild(pos) < heap.size() && heap.get(child_pos).Accumulator > heap.get(rightChild(pos)).Accumulator) {
                child_pos = rightChild(pos);
            }
            if (heap.get(pos).Accumulator <=
                    heap.get(child_pos).Accumulator) {
                break;
            }else {
                swap(pos, child_pos);
                pos = child_pos;
                child_pos = leftChild(pos);
            }
        }
    }

    private void swap(int fpos, int spos) {
        DocAc tmp;
        tmp = heap.get(fpos);
        heap.set(fpos, heap.get(spos));
        heap.set(spos, tmp);
    }

    private int parent(int pos)
    {
        return pos / 2;
    }

    private int leftChild(int pos)
    {
        return (2 * pos);
    }

    private int rightChild(int pos)
    {
        return (2 * pos) + 1;
    }

    private void print()
    {
        for (int i = 1; i <= currentSize / 2; i++ )
        {
            System.out.print(" PARENT : " + heap.get(i).Accumulator + " LEFT CHILD : " + heap.get(2*i).Accumulator
                    + " RIGHT CHILD :" + heap.get(2 * i  + 1).Accumulator);
            System.out.println();
        }
    }

    private void insert(DocAc docAc) {
        heap.add(++currentSize, docAc);
        int current = currentSize;
        while (heap.get(current).Accumulator < heap.get(parent(current)).Accumulator) {
            swap(current, parent(current));
            current = parent(current);
        }
    }

    private String readText(String filename, int docID, int offset) {
        File f = new File(filename);
        if (f.exists() && f.canRead()) {
            try {
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder tagBuilder = new StringBuilder();
                isText = true;
                isTag = false;
                FileInputStream inputStream = new FileInputStream(f);
                inputStream.skip((long) mapping.get(docID)[offset]);
                int content;
                stringBuilder.setLength(0);
                while ((content = inputStream.read()) != -1) {
                    if (content == (int) '<') {
                        isText = false;
                        isTag = true;
                        tagBuilder.setLength(0);
                    }
                    if (isText) stringBuilder.append((char) content);
                    if (isTag) tagBuilder.append((char) content);
                    if (content == (int) '>') {
                        isText = true;
                        isTag = true;
                        if (tagBuilder.indexOf(tagList.get(offset)[1]) > -1) break;
                    }
                }
                inputStream.close();
                return stringBuilder.toString();
            } catch (IOException io) {
                io.printStackTrace();
            }
        } else {
            System.out.println("latimes doesn't exist or can't be read");
        }
        return "";
    }

    private String readText(String filename, int[] arr, int offset) { //offset = 0 for ID, 1 for headline, 2 for text

        File f = new File(filename);
        if (f.exists() && f.canRead()) {
            try {
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder tagBuilder = new StringBuilder();
                isText = true;
                isTag = false;
                for (int i = 0; i < arr.length; i = i + 2) {
                    FileInputStream inputStream = new FileInputStream(f);
                    inputStream.skip((long) mapping.get(arr[i])[offset]);
                    int content;
                    stringBuilder.setLength(0);
                    while ((content = inputStream.read()) != -1) {
                        if (content == (int) '<') {
                            isText = false;
                            isTag = true;
                            tagBuilder.setLength(0);
                        }
                        if (isText) stringBuilder.append((char) content);
                        if (isTag) tagBuilder.append((char) content);
                        if (content == (int) '>') {
                            isText = true;
                            isTag = true;
                            if (tagBuilder.indexOf(tagList.get(offset)[1]) > -1) break;
                        }
                    }
                    System.out.println("DocNo: |" + stringBuilder.toString() + "| " + arr[i + 1]);
                    inputStream.close();
                }
                return stringBuilder.toString();
            } catch (IOException io) {
                io.printStackTrace();
            }
        } else {
            System.out.println("latimes doesn't exist or can't be read");
        }
        return "";
    }

    private void search(String query) throws IOException {
        final int CHAR_SIZE = 4;
        int seekPos;
        int docf;
        RandomAccessFile randomFile = new RandomAccessFile(args[1], "r");

        if (lexicon.containsKey(query)) {
            // use the pointer given by lexicon entry to find the location of term's inverted list
            int pointer = lexicon.get(query)[1];
            seekPos = CHAR_SIZE * pointer;
            randomFile.seek(seekPos);
            docf = randomFile.readInt();
            int arr[] = new int[docf *2];

            for (int i = pointer + 1, j = 0; i <= pointer + (docf * 2) && j< arr.length; i++, j++ ) {
//                seekPos = i * CHAR_SIZE;
//                randomFile.seek(seekPos);
                arr[j] = randomFile.readInt();
            }

            readText(args[3], arr, 0);
        } else {
            System.out.println("No results\n");
        }
    }

    public class DocAc {
        private int DocID;
        private double Accumulator;

        public DocAc(int DocID, double Accumulator) {
            this.DocID = DocID;
            this.Accumulator = Accumulator;
        }
    }

}
