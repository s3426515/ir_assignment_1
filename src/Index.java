/**
 * Created by Master Mind on 3/24/2017.
 */
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;

public class Index {
    private ConcurrentHashMap<Integer, ArrayList<String>> stopTable = new ConcurrentHashMap<>(16, 0.75F, 100000);
    private ConcurrentHashMap<String, int[]> lexicon = new ConcurrentHashMap<>(16, 0.75F, 100000);
    private ConcurrentHashMap<String, ArrayList<Integer>> postingList = new ConcurrentHashMap<>(16, 0.75F, 100000);
    private ConcurrentHashMap<Integer, double[]> mappingTable = new ConcurrentHashMap<>(16, 0.75F, 100000);
    private ConcurrentHashMap<Long, ConcurrentHashMap<String, ArrayList<Integer>>> temp1 = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Long, ConcurrentHashMap<String, int[]>> temp2 = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, String> docLists = new ConcurrentHashMap<>();
    private boolean isRunning = true;
    private ArrayList<String> tempDocs = new ArrayList<>();
    private Tag tags = new Tag();
    private String[] args;
    private int docCounter = 0;
    private String pattern1 = "(?<!\\D)\\,(?!\\D)";                     //match any comma that is between 2 numerical charaters
    private String pattern2 = "[^a-zA-Z0-9_.-]+|(?<!\\d)\\.(?!\\d)";    //match any punctuation that is not hyphen except for dots that are between 2 digits
    private String pattern3 = "(?!\\D\\W)\\.(?!\\d\\w*)";               //match any dot that is between 2 alpha characters
    private boolean printTerms = false;
    private int numberOfThread = 32;
    private boolean shouldPrintLog = true;

    public Index(String[] args) {
        this.args = args;
        start();
    }

    private void start() {
        input();
    }

    private void input() {
        try {
            if (args.length == 1) {
                //TODO - Index document
                parseDoc(args[0]);
            } else if (args.length == 2) {
                if (args[0].equals("-p")) {
                    //TODO - Index document and print out terms
                    printTerms = true;
                    parseDoc(args[1]);
                } else System.out.println("Invalid!!\nFirst argument must be -p followed by file name of document.");
            } else if (args.length == 3) {
                String stoplist = "";
                String doc = "";
                int sOccurence = 0;
                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals("-s")) {
                        sOccurence++;
                        stoplist = args[i + 1];
                        doc = args[(i + 2) % 3];
                    }
                }
                if (sOccurence == 1 && !stoplist.equals("") && !doc.equals("")) {
                    //TODO - Index document with Stop list
                    readStopList(stoplist);
                    parseDoc(doc);
                } else System.out.println("Invalid!!\nThere must be an argument -s followed by the stoplist.\\The document file name can be at first or last.");
            } else if (args.length == 4) {
                String stoplist = "";
                String doc = "";
                int pOccurence = 0, sOccurence = 0;
                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals("-p") && i < 3) {
                        pOccurence++;
                        doc = args[i + 1];
                    } else if (args[i].equals("-s") && i < 3) {
                        sOccurence++;
                        stoplist = args[i + 1];
                    }
                }
                if (pOccurence == 1 && sOccurence == 1 && !stoplist.equals("") && !doc.equals("")) {
                    //TODO - Index document with Stop list and print out terms
                    printTerms = true;
                    readStopList(stoplist);
                    parseDoc(doc);
                } else System.out.println("Invalid!!\n One or more of the 4 arguments might be invalid.");
            } else System.out.println("Invalid!!\nPlease supply at least 1 argument.");
        } catch (IOException e) {
            System.out.println("Something is wrong. One or more files are missing.");
        }
    }

    private void readStopList(String file) throws IOException{
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                int charCode = (int) line.charAt(0);
                if (stopTable.containsKey(charCode))
                    stopTable.get(charCode).add(line);
                else {
                    stopTable.put(charCode, new ArrayList<>());
                    stopTable.get(charCode).add(line);
                }
            }
        }
    }

    private boolean isKeyInStopList(String term) {
        int charCode = (int) term.charAt(0);
        if (stopTable.containsKey(charCode))
            return stopTable.get(charCode).contains(term);
        else
            return false;
    }

    private void updateMappingTable(int docID, long docIdOffset, long titleOffset, long textOffset, long docLength) {
        mappingTable.put(docID, new double[5]);
        mappingTable.get(docID)[0] = docIdOffset;
        mappingTable.get(docID)[1] = titleOffset;
        mappingTable.get(docID)[2] = textOffset;
        mappingTable.get(docID)[3] = docLength;
    }

    private String normalizeString(String source) {
        String docTemp = source.replaceAll(pattern1, "");
        docTemp = docTemp.replaceAll(pattern3, "");
        return docTemp.replaceAll(pattern2, " ").toLowerCase();
    }

    private void parseDoc(String file) throws IOException{
        long startTime = System.nanoTime();
        FileInputStream is = new FileInputStream(file);
        Reader fileReader;
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder tagBuilder = new StringBuilder();
        int charsRead;
        long pointer = 0, docIdOffset = 0, titleOffset = 0, textOffset = 0;
        char buffer[] = new char[1];
        boolean isTag = false;
        boolean isText = false;
        ExecutorService es = Executors.newCachedThreadPool();

        for (int x = 0; x < numberOfThread; x++) {
            final int delimiter = x;
            es.execute(() -> {
                buildListsThread(delimiter);
            });
        }
        try {
            fileReader = new InputStreamReader(is, "UTF-8");

            //Read until there is no more characters to read.
            while ((charsRead = fileReader.read(buffer)) > 0) {
                if (tags.isStartTagExist(tagBuilder.toString())) {
                    if (!tags.getPreviousTag().equals("<TEXT>")) stringBuilder.setLength(0);
                    isText = true;
                    tagBuilder.setLength(0);
                    switch (tags.getPreviousTag()) {
                        case "<DOCNO>":
                            docIdOffset = pointer;
                            break;
                        case "<HEADLINE>":
                            titleOffset = pointer;
                            break;
                        case "<TEXT>":
                            textOffset = pointer;
                            break;
                    }
                } else if (tags.isEndTagExist(tagBuilder.toString())) {
                    //TODO
                    long docLength = stringBuilder.toString().getBytes().length;
                    String doc = normalizeString(stringBuilder.toString().replaceAll("\\<.*?>",""));
                    if (tags.getPreviousTag2().equals("<TEXT>")) {
                        updateMappingTable(docCounter, docIdOffset, titleOffset, textOffset, docLength);
                        docLists.put(docCounter, doc);
                        docCounter++;
                    }
                    isText = false;
                    tagBuilder.setLength(0);
                }
                if (buffer[0] == '<') {
                    isTag = true;
                    tagBuilder.setLength(0);
                }
                if (isTag)
                    tagBuilder.append(buffer, 0, charsRead);
                if (isText)
                    stringBuilder.append(buffer, 0, charsRead);
                if (buffer[0] == '>') {
                    isTag = false;
                }
                pointer++;

            }
            isRunning = false;
            es.shutdown();
            es.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            long endTime = System.nanoTime();
            double totalTime = ((endTime - startTime) / 1000000L) / 60000;
            System.out.println(lexicon.size());
            System.out.println(mappingTable.size());
            System.out.printf("Took: %.2f minutes\n", totalTime);
            System.out.println("");
            fileReader.close();

            writeListToFiles();
        } catch (UnsupportedEncodingException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void buildListsThread(int delimiter) {
        ConcurrentHashMap<String, Integer> temp = new ConcurrentHashMap<>();
        int currentDelimiter = delimiter;
        printLog("Thread " + delimiter + " started.");
        while (isRunning || docLists.containsKey(currentDelimiter)) {
            if (docLists.containsKey(currentDelimiter)) {
//                int docID = (int) Math.floorDiv(currentDelimiter, 2) + 1;
//                printLog("Thread " + delimiter + " processing doc no." + currentDelimiter);
                buildLists(docLists.get(currentDelimiter), currentDelimiter, temp);
                mappingTable.get(currentDelimiter)[4] = calculateDocWeight(temp);
                docLists.remove(currentDelimiter);
                currentDelimiter += numberOfThread;
            }
            temp.clear();
        }
    }

    private void buildLists(String source, int docCounter, ConcurrentHashMap<String, Integer> hashmap) {
        try {
            String[] arr = source.split("\\s");
            for (String anArr : arr) {
                if (!anArr.equals("")) {
                    if (!isKeyInStopList(anArr)) {
                        //Update the lexicon and posting list
                        if (lexicon.containsKey(anArr)) {
                            int existingValue = 0;
                            //if the doc has an entry in the posting list for this term, update it
                            for (int a = 1; a < postingList.get(anArr).size(); a += 2) {
                                synchronized (postingList.get(anArr)) {
                                    if (postingList.get(anArr).get(a) == docCounter) {
                                        if (postingList.get(anArr).get(a + 1) == null)
                                            System.out.println("Thread " + docCounter + "has null for " + postingList.get(anArr) + "\nTry to access " + (a + 1) + " element when size is " + postingList.get(anArr).size());
                                        int temp = postingList.get(anArr).get(a + 1);
                                        postingList.get(anArr).set(a + 1, temp + 1);
                                        existingValue++;
                                        break;
                                    }
                                }

                            }
                            //if not, create a new entry and update the number of doc
                            if (existingValue == 0) {
                                synchronized (postingList.get(anArr)) {
                                    postingList.get(anArr).add(docCounter);
                                    postingList.get(anArr).add(1);
                                    int temp = postingList.get(anArr).get(0);
                                    postingList.get(anArr).set(0, temp + 1);
                                    lexicon.get(anArr)[0] = temp + 1;
                                }
                            }
                        } else {
                            if (printTerms) System.out.println("Processing term: " + anArr);
                            synchronized (postingList) {
                                postingList.put(anArr, new ArrayList<>());
                                postingList.get(anArr).add(1);
                                postingList.get(anArr).add(docCounter);
                                postingList.get(anArr).add(1);
                            }
                            lexicon.put(anArr, new int[2]);
                            lexicon.get(anArr)[0] = 1;
                        }

                        //Update the hash table for in-document frequency of terms
                        if (hashmap.containsKey(anArr)) {
                            int temp = hashmap.get(anArr);
                            hashmap.put(anArr, temp + 1);
                        } else {
                            hashmap.put(anArr, 1);
                        }
                    }
                }
            }

        } catch (NullPointerException e) {e.printStackTrace();}
    }

    private double calculateDocWeight(ConcurrentHashMap<String, Integer> temp) {
        double weight = 0;
        for (HashMap.Entry<String, Integer> entry : temp.entrySet()) {
            weight += mathSquare(1 + Math.log(entry.getValue()));
        }
        weight = Math.sqrt(weight);
        return weight;
    }

    private double mathSquare(double a) {
        return a*a;
    }

    private void writeListToFiles() throws IOException {
        //Write posting list and update lexicon
        long startTime = System.nanoTime();
        printLog("Writing posting list to file...");
        RandomAccessFile raf = new RandomAccessFile("invlists", "rw");
        int pointer = 0;
        for (HashMap.Entry<String, ArrayList<Integer>> entry : postingList.entrySet()) {
            lexicon.get(entry.getKey())[1] = pointer;
            for (int a : entry.getValue()) {
                pointer++;
                raf.writeInt(a);
            }
        }
        raf.close();

        //Write lexicon
        printLog("Writing lexicon to file...");
        FileWriter fw1 = new FileWriter("lexicon");
        String newLine = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        for (HashMap.Entry<String, int[]> entry : lexicon.entrySet()) {
            sb.setLength(0);
            sb.append(entry.getKey());
            for (int i = 0; i < entry.getValue().length; i++) {
                sb.append(",");
                sb.append(entry.getValue()[i]);
            }
            sb.append(newLine);
            fw1.write(sb.toString());
        }
        fw1.close();

        //Write mapping table
        printLog("Writing mapping table to file...");
        FileWriter fw2 = new FileWriter("map");
        for (HashMap.Entry<Integer, double[]> entry : mappingTable.entrySet()) {
            sb.setLength(0);
            sb.append(entry.getKey());
            for (double offset : entry.getValue()) {
                sb.append(",");
                sb.append(new BigDecimal(offset).toPlainString());
            }
            sb.append(newLine);
            fw2.write(sb.toString());
        }
        fw2.close();


        long endTime = System.nanoTime();
        double totalTime = ((endTime - startTime) / 1000000L) / 60000;
        System.out.printf("Took: %.2f minutes\n", totalTime);
        System.out.println("");
    }

    private void printLog(String msg) {
        if (shouldPrintLog) System.out.println(msg);
    }

    private void printLog(String val1, String val2) {
        if (shouldPrintLog) System.out.printf(val1, val2);
    }

    public static void main(String[] args) {
        Index m = new Index(args);
    }
}
