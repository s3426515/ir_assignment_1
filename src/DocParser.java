import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
/**
 * Created by Master Mind on 4/9/2017.
 */
public class DocParser implements Runnable{
    private String threadName;
    private String source;
    private int docCounter;
    private boolean printTerms;
    private Thread t;
    private ConcurrentHashMap<Integer, ArrayList<String>> stopTable;
    private ConcurrentHashMap<String, int[]> lexicon;
    private ConcurrentHashMap<String, ArrayList<Integer>> postingList;
    private ConcurrentHashMap<Integer, long[]> mappingTable;

    public DocParser(String threadName, String source, ConcurrentHashMap<Integer, ArrayList<String>> stopTable, ConcurrentHashMap<String, int[]> lexicon, ConcurrentHashMap<String, ArrayList<Integer>> postingList, ConcurrentHashMap<Integer, long[]> mappingTable, int docCounter, boolean printTerms) {
        this.threadName = threadName;
        this.stopTable = stopTable;
        this.lexicon = lexicon;
        this.postingList = postingList;
        this.mappingTable = mappingTable;
        this.docCounter = docCounter;
        this.printTerms = printTerms;
        this.source = source;
    }

    private boolean isKeyInStopList(String term) {
        int charCode = (int) term.charAt(0);
        if (stopTable.containsKey(charCode))
            return stopTable.get(charCode).contains(term);
        else
            return false;
    }

    @Override
    public void run() {
        String[] arr = source.split("\\s");
        for (String anArr : arr) {
            if (!anArr.equals("")) {
                if (!isKeyInStopList(anArr)) {
                    if (lexicon.containsKey(anArr)) {
                        int existingValue = 0;
                        //if the doc has an entry in the posting list for this term, update it
                        for (int a = 0; a < postingList.get(anArr).size(); a++) {
                            if ((a % 2 == 1) && (postingList.get(anArr).get(a) == docCounter)) {
                                int temp = postingList.get(anArr).get(a + 1);
                                postingList.get(anArr).set(a + 1, temp + 1);
                                existingValue++;
                                break;
                            }
                        }
                        //if not, create a new entry and update the number of doc
                        if (existingValue == 0) {
                            postingList.get(anArr).add(docCounter);
                            postingList.get(anArr).add(1);
                            int temp = postingList.get(anArr).get(0);
                            postingList.get(anArr).set(0, temp + 1);
                            lexicon.get(anArr)[0] = temp + 1;
                        }
                    } else {
                        if (printTerms) System.out.println("Processing term: " + anArr);
                        postingList.put(anArr, new ArrayList<>());
                        lexicon.put(anArr, new int[2]);

                        postingList.get(anArr).add(1);
                        postingList.get(anArr).add(docCounter);
                        postingList.get(anArr).add(1);
                        lexicon.get(anArr)[0] = 1;
                    }
                }
            }
        }
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
