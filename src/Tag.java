/**
 * Created by Master Mind on 3/26/2017.
 */

import java.util.HashMap;

public class Tag {
    private HashMap<String, String> tags = new HashMap<>();
    private String prevTag = "";
    private String prevTag2 = "";

    public Tag() {
        tags.put("<TEXT>", "</TEXT>");
        tags.put("<DOCNO>", "</DOCNO>");
        tags.put("<HEADLINE>", "</HEADLINE>");
    }

    public boolean isStartTagExist(String tag) {
        boolean bool = tags.containsKey(tag);
        if (bool) prevTag = tag;
        return bool;
    }

    public boolean isEndTagExist(String tag) {
        boolean bool = tag.equals(tags.get(prevTag));
        if (bool) {prevTag2 = prevTag; prevTag = "";}
        return bool;
    }

    public String getPreviousTag2() {
        return prevTag2;
    }

    public String getPreviousTag() {
        return prevTag;
    }
}
